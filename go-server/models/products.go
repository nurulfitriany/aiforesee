package models

import (
	"github.com/jinzhu/gorm"
)

type ProductsItem struct {
	gorm.Model
	Quantity  int `json:"Quantity"`
	Pertalite int `json:"Pertalite"`
	Pertamax  int `json:"Pertamax"`
}
