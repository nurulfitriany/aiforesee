package main

import (
	"fmt"
	"go-server/db"
	"go-server/handlers"
	"go-server/models"
	"io"
	"net/http"

	seeders "go-server/db/seeders"

	. "os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func init() {
	file, err := OpenFile("server.log", O_RDWR|O_CREATE|O_APPEND, 0666)
	if err != nil {
		fmt.Println("Could not open file with error: " + err.Error())
	}

	log.SetOutput(file)
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
}

func main() {

	log.Info("Connecting to PostgreSQL DB...")
	db := db.DBInit()
	defer db.Close()
	db.DropTableIfExists(&models.ProductsItem{})
	db.AutoMigrate(&models.ProductsItem{})

	log.Info("Starting Server")
	router := mux.NewRouter()

	log.Info("Starting insert seeder")
	seeders.DBSeed(db)

	router.HandleFunc("/healthcheck", healthCheckHandler).Methods("GET")
	router.HandleFunc("/api/v1/product", handlers.CreateProductHandler(db)).Methods("POST")
	router.HandleFunc("/api/v1/products", handlers.GetProductsHandler(db)).Methods("GET")
	router.HandleFunc("/api/v1/product/{id}", handlers.GetProductByIdHandler(db)).Methods("GET")
	router.HandleFunc("/api/v1/product/{id}", handlers.UpdateProductHandler(db)).Methods("PUT")
	router.HandleFunc("/api/v1/product/{id}", handlers.DeleteProductHandler(db)).Methods("DELETE")
	http.ListenAndServe(":8000", router)
}

func healthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	io.WriteString(w, `{"alive": true}`)
}
