package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	model "go-server/models"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

/*
	CREATE NEW PRODUCT
*/
func CreateProductHandler(db *gorm.DB) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		quantity, _ := strconv.Atoi(r.FormValue("quantity"))
		pertalite, _ := strconv.Atoi(r.FormValue("pertalite"))
		pertamax, _ := strconv.Atoi(r.FormValue("pertamax"))

		newProduct := &model.ProductsItem{Quantity: quantity, Pertalite: pertalite, Pertamax: pertamax}

		db.Create(&newProduct)
		fmt.Println("Inserted a Single Record")

		res := Result{Code: 200, Data: newProduct, Message: "Successfully create product"}
		result, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}

	return http.HandlerFunc(fn)
}

/*
	GET LIST OF PRODUCT
*/
func GetProductsHandler(db *gorm.DB) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		products := []model.ProductsItem{}

		db.Find(&products)
		fmt.Println("Getting List of Products")

		res := Result{Code: 200, Data: products, Message: "Successfully get list of product"}
		result, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}
	return http.HandlerFunc(fn)
}

/*
	GET PRODUCT DETAIL
*/
func GetProductByIdHandler(db *gorm.DB) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var product model.ProductsItem

		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["id"])

		db.Where("id = ?", id).Find(&product)
		fmt.Println("Getting Product Detail")

		res := Result{Code: 200, Data: product, Message: "Successfully get detail of product"}
		result, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}
	return http.HandlerFunc(fn)
}

/*
	UPDATE PRODUCT
*/
func UpdateProductHandler(db *gorm.DB) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var product model.ProductsItem

		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["id"])

		quantity, _ := strconv.Atoi(r.FormValue("quantity"))
		pertalite, _ := strconv.Atoi(r.FormValue("pertalite"))
		pertamax, _ := strconv.Atoi(r.FormValue("pertamax"))

		db.Where("id = ?", id).Find(&product)

		product.Quantity = quantity
		product.Pertalite = pertalite
		product.Pertamax = pertamax

		db.Save(&product)
		fmt.Println("Successfully Updating Data Product")

		res := Result{Code: 200, Data: product, Message: "Successfully update product"}
		result, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}
	return http.HandlerFunc(fn)
}

/*
	DELETE PRODUCT
*/
func DeleteProductHandler(db *gorm.DB) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var product model.ProductsItem

		vars := mux.Vars(r)
		id, _ := strconv.Atoi(vars["id"])

		db.Where("id = ?", id).Find(&product)

		db.Delete(&product)
		fmt.Println("Successfully Delete Data Product")

		res := Result{Code: 200, Message: "Successfully delete product"}
		result, err := json.Marshal(res)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(result)
	}
	return http.HandlerFunc(fn)
}
