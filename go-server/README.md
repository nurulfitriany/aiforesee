# Getting Started with Application

In the project directory /go-server, you can run:

### `go run main.go`

PostgreSQL installations. You can use Docker to run a database easily.

### `docker run -d --name postgres-aiforesee -p 5430:5432 -e POSTGRES_PASSWORD=password -v ${PWD}/database:/var/lib/postgresql/data postgres:12`

### `docker exec -it postgres-aiforesee bash`

## List of API
* Creating a new product,
* Updating an existing product,
* Deleting an existing product,
* Fetching a list of products.