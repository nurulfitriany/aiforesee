package db

import (
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

func DBInit() *gorm.DB {
	log.Info("Starting Database Connection")

	db, err := gorm.Open("postgres", "host=localhost port=5430 user=postgres dbname=postgres password=password sslmode=disable")
	if err != nil {
		log.Panic("Failed to connect database with error " + err.Error())
	}

	db.LogMode(true)
	return db
}
