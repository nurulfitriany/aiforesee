package seeders

import (
	"go-server/db/fakers"

	"github.com/jinzhu/gorm"
)

type Seeders struct {
	Seeders interface{}
}

func RegisterSeeders(db *gorm.DB) []Seeders {
	return []Seeders{
		{Seeders: fakers.ProductFaker(1)},
		{Seeders: fakers.ProductFaker(2)},
		{Seeders: fakers.ProductFaker(3)},
		{Seeders: fakers.ProductFaker(4)},
		{Seeders: fakers.ProductFaker(5)},
		{Seeders: fakers.ProductFaker(6)},
		{Seeders: fakers.ProductFaker(7)},
		{Seeders: fakers.ProductFaker(8)},
		{Seeders: fakers.ProductFaker(9)},
		{Seeders: fakers.ProductFaker(10)},
		{Seeders: fakers.ProductFaker(11)},
		{Seeders: fakers.ProductFaker(12)},
		{Seeders: fakers.ProductFaker(13)},
		{Seeders: fakers.ProductFaker(14)},
		{Seeders: fakers.ProductFaker(15)},
		{Seeders: fakers.ProductFaker(16)},
		{Seeders: fakers.ProductFaker(17)},
		{Seeders: fakers.ProductFaker(18)},
		{Seeders: fakers.ProductFaker(19)},
		{Seeders: fakers.ProductFaker(20)},
	}

}

func DBSeed(db *gorm.DB) error {
	for _, seeder := range RegisterSeeders(db) {
		err := db.Debug().Create(seeder.Seeders).Error
		if err != nil {
			return err
		}
	}
	return nil
}
