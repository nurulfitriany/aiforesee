package fakers

import (
	"go-server/models"
)

func ProductFaker(i int) *models.ProductsItem {
	return &models.ProductsItem{
		Quantity:  i,
		Pertalite: 6450 * i,
		Pertamax:  7650 * i,
	}
}
