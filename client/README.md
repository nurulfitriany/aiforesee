# Getting Started with Create React App
## Available Scripts

In the project directory [../client], you can run:

### `npm install | npm i --verbose`

then run
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

