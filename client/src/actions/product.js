import { 
    RETRIEVE_PRODUCT_SUCCESS,
    RETRIEVE_PRODUCT_STARTED,
    RETRIEVE_PRODUCT_FAILURE,
    CREATE_PRODUCT_SUCCESS,
    CREATE_PRODUCT_STARTED,
    CREATE_PRODUCT_FAILURE,
    UPDATE_PRODUCT_SUCCESS,
    UPDATE_PRODUCT_STARTED,
    UPDATE_PRODUCT_FAILURE
} from "./types/product";

import axios from 'axios';

export const getProductDetail = (data) => {
    return (dispatch, getState) => {
      dispatch(retrieveProductStarted());
      console.log('current state product detail', getState());

      axios
        .get(`/api/v1/product/${data.id}`, {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              "Access-Control-Allow-Origin": "*",
            }
          })
        .then(res => {
          dispatch(retrieveProductSuccess(res.data));
        })
        .catch(err => {
          dispatch(retrieveProductFailure(err.message));
        });
    }
}
export const createProduct = (data) => {
  return (dispatch, getState) => {
    dispatch(createProductStarted());
    console.log('current state create product', getState());
    
    axios
      .post(`/api/v1/product`, new URLSearchParams({
        quantity: data.quantity,
        pertalite: data.pertalite,
        pertamax: data.pertamax
      }))
      .then(res => {
        dispatch(createProductSuccess(res.data));
      })
      .catch(err => {
        dispatch(createProductFailure(err.message));
      });
  }
}

export const updateProduct = (id, data) => {
  return (dispatch, getState) => {
    dispatch(updateProductStarted());
    console.log('current state update product', getState());
  
    axios
      .put(`/api/v1/product/${id.id}`, new URLSearchParams({
        quantity: data.quantity,
        pertalite: data.pertalite,
        pertamax: data.pertamax
      }))
      .then(res => {
        dispatch(updateProductSuccess(res.data));
      })
      .catch(err => {
        dispatch(updateProductFailure(err.message));
      });
  }
}

const retrieveProductSuccess = product => ({
    type: RETRIEVE_PRODUCT_SUCCESS,
    payload: {
      ...product
    }
});
  
const retrieveProductStarted = () => ({
    type: RETRIEVE_PRODUCT_STARTED
});
  
const retrieveProductFailure = error => ({
    type: RETRIEVE_PRODUCT_FAILURE,
    payload: {
      error
    }
});

const createProductSuccess = product => ({
  type: CREATE_PRODUCT_SUCCESS,
  payload: {
    ...product
  }
});

const createProductStarted = () => ({
  type: CREATE_PRODUCT_STARTED
});

const createProductFailure = error => ({
  type: CREATE_PRODUCT_FAILURE,
  payload: {
    error
  }
});

const updateProductSuccess = product => ({
  type: UPDATE_PRODUCT_SUCCESS,
  payload: {
    ...product
  }
});

const updateProductStarted = () => ({
  type: UPDATE_PRODUCT_STARTED
});

const updateProductFailure = error => ({
  type: UPDATE_PRODUCT_FAILURE,
  payload: {
    error
  }
});
