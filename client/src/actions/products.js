import { 
    RETRIEVE_PRODUCTS_SUCCESS,
    RETRIEVE_PRODUCTS_STARTED,
    RETRIEVE_PRODUCTS_FAILURE,
    DELETE_PRODUCTS_SUCCESS,
    DELETE_PRODUCTS_STARTED,
    DELETE_PRODUCTS_FAILED
  } from "./types/products";

import axios from 'axios';

export const getProducts = () => {
    return (dispatch, getState) => {
        dispatch(retrieveProductsStarted());
        console.log('current state get products:', getState());

        axios
        .get(`/api/v1/products`, {
            headers: {
                "Content-type": "application/x-wwww-form-urlencoded",
                "Access-Control-Allow-Origin": "*",
            }
            })
        .then(res => {
            dispatch(retrieveProductsSuccess(res.data));
        })
        .catch(err => {
            dispatch(retrieveProductsFailure(err.message));
        });
        
    };
};

export const deleteProducts = (id) => {
    return (dispatch, getState) => {
        dispatch(deleteStarted());
        console.log('state for delete', getState());

        axios
        .delete(`/api/v1/product/${id}`, {
            headers: {
                "Content-type": "application/x-wwww-form-urlencoded",
                "Access-Control-Allow-Origin": "*",
            }
            })
        .then(res => {
            dispatch(deleteSuccess(res.data));
        })
        .catch(err => {
            dispatch(deleteFailure(err.message));
        });
        
    };
  };

const retrieveProductsSuccess = products => ({
    type: RETRIEVE_PRODUCTS_SUCCESS,
    payload: {
      ...products
    }
});

const retrieveProductsStarted = () => ({
    type: RETRIEVE_PRODUCTS_STARTED
});

const retrieveProductsFailure = error => ({
    type: RETRIEVE_PRODUCTS_FAILURE,
    payload: {
        error
    }
});

const deleteStarted = () => ({
    type: DELETE_PRODUCTS_STARTED
});

const deleteSuccess = success => ({ 
    type: DELETE_PRODUCTS_SUCCESS,
    payload: {
      ...success
    }
});

const deleteFailure = error => ({
    type: DELETE_PRODUCTS_FAILED,
    payload: {
        error
    }
});