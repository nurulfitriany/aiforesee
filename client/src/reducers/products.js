import {
    RETRIEVE_PRODUCTS_SUCCESS,
    RETRIEVE_PRODUCTS_STARTED,
    RETRIEVE_PRODUCTS_FAILURE,
    DELETE_PRODUCTS_SUCCESS,
    DELETE_PRODUCTS_STARTED,
    DELETE_PRODUCTS_FAILED
} from "../actions/types/products";

const initialState = {
    loading: false,
    products: [],
    error: null
};

function productsReducer(state = initialState, action) {
    switch (action.type) {
      case RETRIEVE_PRODUCTS_STARTED:
        return {
          ...state,
          loading: true
        };
      
      case RETRIEVE_PRODUCTS_SUCCESS:
        return {
          ...state,
          loading: false,
          error: null,
          products: action.payload.data
        };

      case RETRIEVE_PRODUCTS_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
      
      case  DELETE_PRODUCTS_STARTED:
        return {
          ...state,
          loading: true
        };
      
      case DELETE_PRODUCTS_SUCCESS:
        return {
          ...state,
          loading: false,
          error: null,
          success: action.payload
        };
      
      case DELETE_PRODUCTS_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
  
      default:
        console.log('default product list')
        return state;
    }
    
};

export default productsReducer;