import React, { Component } from "react";
import { connect } from "react-redux";
import { getProducts, deleteProducts } from "../actions/products";

class ProductsList extends Component {
    

    componentDidMount() {
      this.props.getProducts();
    }

    handleRemoveProduct(id) {
      this.props.deleteProducts(id)
      window.location.reload(false)      
    }
    
    render() {
        const { products: {products}, deletedItems } = this.props;
        console.log("list products", products)
        console.log("deletedItems", deletedItems)
        return (
            
            <div class="table-responsive custom-table-responsive">
                <table class="table custom-table">
                <thead>
                    <tr>  
                    <th scope="col">Quantity</th>
                    <th scope="col">Pertalite</th>
                    <th scope="col">Pertamax</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                {
                    products.map((i) => {
                        return (
                            <tbody>
                                <tr scope="row">
                                    <td><a>{i.Quantity}</a></td>
                                    <td><a href={`/productdetail/${i.ID}`}>{i.Pertalite}</a></td>
                                    <td><a href={`/productdetail/${i.ID}`}>{i.Pertamax}</a></td>
                                    <td>
                                        <button 
                                          onClick={this.handleRemoveProduct.bind(this, i.ID)}
                                          type="button" 
                                          class="btn btn-secondary"
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr> 
                            </tbody>
                        )
                    })
                }
                
                </table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      products: state.products,
      deletedItems: state.deletedItems
    };
  };

export default connect(mapStateToProps, { getProducts, deleteProducts })(ProductsList);