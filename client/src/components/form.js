import React, { Component } from "react";
import { connect } from "react-redux";

import {createProduct} from "../actions/product";

class Form extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
          id: null,
          quantity: null,
          pertalite: null,
          pertamax: null
        };

        this.onChangeQuantity = this.onChangeQuantity.bind(this);
        this.onChangePertalite = this.onChangePertalite.bind(this);
        this.onChangePertamax = this.onChangePertamax.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
      }

      onChangeQuantity(e) {
        this.setState({
            quantity: e.target.value
        });
      }

      onChangePertalite(e) {
        this.setState({
            pertalite: e.target.value
        });
      }

      onChangePertamax(e) {
        this.setState({
            pertamax: e.target.value
        });
      }
            
      onSubmitHandler(e) {
        e.preventDefault();
        // console.log('test', this.state)
        // const { quantity, pertalite, pertamax } = this.state;
        console.log('onFormSubmit : ', this.state); 
        const data = {
            quantity: this.state.quantity,
            pertalite: this.state.pertalite,
            pertamax: this.state.pertamax
        }
        this.props.createProduct(data)
        this.props.history.push('/products')
      }
    render() {
       
        return (
            <div className="edit-form">
            <h4>FORM</h4>
                <form>
                <div className="form-group">
                    <label htmlFor="quantity">Quantity</label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.quantity}
                        onChange={this.onChangeQuantity}
                      />
                </div>
                <div className="form-group">
                    <label htmlFor="pertalite">Pertalite</label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.pertalite}
                        onChange={this.onChangePertalite}
                      />
                </div>

                <div className="form-group">
                    <label htmlFor="pertamax">Pertamax</label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.pertamax}
                        onChange={this.onChangePertamax}
                      />
                </div>

                <div className="form-group">
                    <button type="button" onClick={this.onSubmitHandler}  class="btn btn-success">Save</button>
                </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log("state", state)
    return {
      quantity: state.quantity,
      pertalite: state.pertalite,
      pertamax: state.pertamax
    };
  };

export default connect(mapStateToProps, { createProduct })(Form);