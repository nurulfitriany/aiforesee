import React, { Component } from "react";
import { connect } from "react-redux";

import { getProductDetail, updateProduct } from "../actions/product";

class ProductView extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
        id: null,
        quantity: null,
        pertalite: null,
        pertamax: null
      };

      this.onChangeQuantity = this.onChangeQuantity.bind(this);
      this.onChangePertalite = this.onChangePertalite.bind(this);
      this.onChangePertamax = this.onChangePertamax.bind(this);
      this.onSubmitHandler = this.updateHandler.bind(this);
    }

    onChangeQuantity(e) {
      this.setState({
        quantity: e.target.value
    });
    }

    onChangePertalite(e) {
      this.setState({
        pertalite: e.target.value
      });
    }

    onChangePertamax(e) {
      this.setState({
        pertamax: e.target.value
      });
    }

    componentDidMount() {
      this.props.getProductDetail(this.props.match.params);
    }

    updateHandler(e) {
      e.preventDefault();
      const idProduct = this.props.match.params;
      const data = {
          quantity: this.state.quantity,
          pertalite: this.state.pertalite,
          pertamax: this.state.pertamax
      }
      this.props.updateProduct(idProduct, data)
      this.props.history.push('/products')
    }

    render() {
        const { product: {product} } = this.props;
        console.log("get detail", product)

        return (
          <div className="edit-form">
          <h4>Product Detail</h4>
            <form >
              <div className="form-group">
                  <label htmlFor="quantity">Quantity</label>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={product.Quantity}
                      onChange={this.onChangeQuantity}
                    />
              </div>
              <div className="form-group">
                  <label htmlFor="pertalite">Pertalite</label>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={product.Pertalite}
                      onChange={this.onChangePertalite}
                    />
              </div>

              <div className="form-group">
                  <label htmlFor="pertamax">Pertamax</label>
                    <input
                      type="text"
                      className="form-control"
                      defaultValue={product.Pertamax}
                      onChange={this.onChangePertamax}
                    />
              </div>

              <div className="form-group">
                  <button type="button" onClick={this.onSubmitHandler}  class="btn btn-success">Update</button>
              </div>
            </form>
          </div>
      )
    }
}
const mapStateToProps = (state) => {
    return {
      product: state.product,
      quantity: state.quantity,
      pertalite: state.pertalite,
      pertamax: state.pertamax
    };
  };

export default connect(mapStateToProps, { getProductDetail, updateProduct })(ProductView);